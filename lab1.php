<?php 

include_once 'DBConnector.php';
include_once 'user.php';
include_once 'fileUploader.php';
$cdb = new DBConnector();

if (isset($_POST['btn-save'])) {
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$city_name = $_POST['city_name'];
	$username = $_POST['username'];
	$_SESSION['username']=$username;
	$password = $_POST['password'];
	
	$fileName=$_FILES['fileToUpload']['name'];
	$fileSize=$_FILES['fileToUpload']['size'];
	$fileType = strtolower(pathinfo($fileName,PATHINFO_EXTENSION));
	$finalName=$_FILES['fileToUpload']['tmp_name'];
	
	$utc_timestamp = $_POST['utc_timestamp'];
	$offset = $_POST['time_zone_offset'];

	$user = new User($first_name,$last_name,$city_name,$username,$password);
	$user->setUtcTimestamp($utc_timestamp);
	$user->setOffset($offset);
	
	
	$fileUploader = new fileUploader();

	$fileUploader->setOriginalName($fileName);
	$fileUploader->setType($fileType);
	$fileUploader->setSize($fileSize);
	$fileUploader->setFinalName($finalName);
	$fileUploader->setUsername($username);
	

	if (!$user->validateForm()) {
		$user->createFormErrorSessions();
		header("Refresh:0");
		die();
	}else{
		if ($fileUploader->fileWasSelected()) {
			
			if ($fileUploader->fileTypeisCorrect()) {
				
				if ($fileUploader->fileSizeIsCorrect()) {
					

					if (!($fileUploader->fileAlreadyExists())) {
						
				    $user->save();
					$fileUploader->uploadFile() ;

					}else{
						echo "File already exists"."<br>";

					}

				}else{
					echo "PICK A SMALLER SIZE"."<br>";
				}

			}else{
				echo "INCORRECT TYPE"."<br>";
			}


		}else{echo "NO FILE DETECTED"."<br>";}
	}
    $cdb->closeDatabase();

}

?>
<html>
  <head>
    <title>Lab 1 Form</title>
    <script type = "text/javascript" src = "validate.js"></script>
    <link rel ="stylesheet" type = "text/css" href = "validate.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script type = "text/javascript" src ="timezone.js"></script>
   </head>
   <body>
   <div id="content">
  
       <form method="post" name="user_details" id = "user_details" onsubmit="return validateForm()" action ="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
           <table align="center">
           <tr>
           <td>
               <div id = "form-errors">
                   <?php
                   session_start();
                   if(!empty($_SESSION['form_errors'])){
                       echo " " . $_SESSION['form_errors'];
                       unset($_SESSION['form_errors']);
                   }
                   ?>
               </div>
           </td>
           </tr>
               <tr>
                   <td><input type ="text" name = "first_name"required placeholder = "First Name"/></td>
               </tr>
               <tr>
                   <td><input type = "text" name = "last_name" placeholder = "Last Name"/></td>
               </tr>
               <tr>
                   <td><input type = "text" name = "city_name" placeholder ="City"/></td>
</tr>
<tr>
    <td><input type = "text" name = "username" placeholder = "Username"/></td>
</tr>
<tr>
    <td><input type = "password" name = "password" placeholder = "password" /></td>
</tr>
<tr>
<td>Profile Image:<input type = "file" name = "fileToUpload" id = "fileToUpload"></td>
</tr>
<tr>
    <td><button type = "submit" name = "btn-save" id ="submit"><strong>SAVE</strong></button></td>
</tr>
<input type="hidden" name="utc_timestamp" id="utc_timestamp" value= ""/>
<input type="hidden" name="time_zone_offset" id="time_zone_offset" value= ""/>

<tr>
    <td><a href = "login.php">Login</a></td>
</tr>
           </table>
       </form>
   </body>
</html>