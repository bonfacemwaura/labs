<?php
include "crud.php";
include "authenticator.php";
include_once 'DBconnector.php';
class User implements Crud, authenticator{
    private $user_id;
    private $first_name;
    private $last_name;
    private $city_name;
    private $username;
    private $password;
    private $utc_timestamp;
    private $offset;
   
    function __construct($first_name, $last_name, $city_name, $username, $password){
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->city_name = $city_name;
        $this->username = $username;
        $this->password= $password;
      
        }
        public static function create(){
            $instance = new ReflectionClass(__CLASS__);
            return $instance->newInstanceWithoutConstructor();
        }
            
     
    
public function setUserId($user_id){
    $this->user_id = $user_id;
}
  public function getUserId(){
      return $this->user_id;
  }  
  public function setUserName($username){
    $this->username = $username;
}
public function getUsername(){
    return $this-> username;
}
public function setPassword($password){
    $this->password = $password;
}
public function getPassword(){
   return $this-> password;
}
public function setUtcTimestamp($utc_timestamp){
    $this->utc_timestamp=$utc_timestamp;
}  
public function getUtcTimestamp(){
    return $this->utc_timestamp;
}
public function setOffset($offset){
  $this->offset=$offset;
}  
public function getOffset(){
  return $this->offset;
}

  
public function save(){
    $fn = $this->first_name;
    $ln = $this->last_name;
    $city = $this->city_name;
    $uname = $this->getUsername();
    $this->hashpassword();
    $pass = $this->getPassword();
    $utc= $this->getUtcTimestamp();
    $off=$this->getOffset();

    
    $res = "INSERT INTO user (first_name, last_name, user_city, username, password, time_utc, offset)
    VALUES ('$fn', '$ln', '$city','$uname','$pass', '$utc','$off')" or die("Error" .$this->conn->connection_error);
    $conn = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    $conn->query($res);
        
    return $res;
}
public function readAll(){
    $sql = "SELECT * from user";
    $conn = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    $conn->query($sql);
    if ($sql->num_rows > 0) {
        // output data of each row
        while($row = $sql->fetch_assoc()) {
            echo "id: " . $row["id"]. " - Name: " . $row["first_name"]. " " . $row["last_name"]. " " .$row["user_city"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    return $sql;
}
public function readUnique(){
    return null;
}
public function search(){
    return null;
}
public function update(){
    return null;
}
public function removeOne(){
    return null;
}
public function removeAll(){
    return null;
}
public function validateForm()
{
    $fn = $this->first_name;
    $ln = $this ->last_name;
    $city = $this ->city_name;
    
    if($fn == "" || $ln == "" || $city == ""){
        return false;
    }
    return true;
}
public function createFormErrorSessions()
{
    session_start();
    $_SESSION['form_errors']="All fields ae required";

}
public function hashPassword()
{ 

    $this->password = password_hash($this->password, PASSWORD_DEFAULT);
}
 public function isPasswordCorrect(){
           $con=new DBconnector;
            $found = false;
            $sql= "SELECT * from user";
            $conn = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            $res=$conn->query($sql);
            
            while($row =mysqli_fetch_array($res)){
                if(password_verify($this->getPassword(),$row['password'])&& $this->getUsername() == $row['username']){
                    $found=true;
                }
            }
            $con->closeDatabase();
            return $found;
        }
    
    public function login(){
        if($this->isPasswordCorrect()){
            header("Location:private_page.php");
        }
}
public function createUserSession(){
    session_start();
    $_SESSION['username'] = $this ->getUsername();
}
public function logout(){
    session_start();
    unset($_SESSION['username']);
    session_destroy();
    header("Location:lab1.php");
}
}

?>
